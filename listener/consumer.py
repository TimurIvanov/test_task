from kafka import KafkaConsumer
import pickle
import requests


def cons():
    consumer = KafkaConsumer('msg_topic', bootstrap_servers='127.0.0.1:9092')

    for message in consumer:
        deserialized_data = pickle.loads(message.value)
        result = deserialized_data['msg'].lower().find('абракадабра')
        if result == -1:
            result = False
        else:
            result = True

        r = requests.post('http://127.0.0.1:8000/api/v1/message_confirmation/',
                          data={'message_id': deserialized_data['msg_id'],
                                'success': result},
                          headers={'Authorization': f"{deserialized_data['msg_token']}"}
                          )


if __name__ == "__main__":
    while True:
        cons()
