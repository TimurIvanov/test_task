from django.db import models


class CustomUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, null=True)


class Message(models.Model):
    BLOCKED = 'blocked'
    REVIEW = 'review'
    CORRECT = 'correct'
    MESSAGE_STATUS = [
        (BLOCKED, 'Заблокировано'),
        (REVIEW, 'На проверке'),
        (CORRECT, 'Корректно')
    ]

    id = models.BigAutoField(primary_key=True)
    text = models.TextField(max_length=512, blank=True, null=True)
    status = models.CharField(max_length=20, choices=MESSAGE_STATUS, default=REVIEW, null=False)
    user = models.ForeignKey(CustomUser, on_delete=models.DO_NOTHING, related_name='message', null=True)


class MessageToken(models.Model):
    id = models.BigAutoField(primary_key=True)
    token = models.CharField(max_length=100, null=False)
    message = models.ForeignKey(Message, on_delete=models.DO_NOTHING, null=True)
