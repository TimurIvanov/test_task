from django.urls import path
from .views import MessageView, MessageConfirmationView

urlpatterns = [
    path('message/', MessageView.as_view(), name="message"),
    path('message_confirmation/', MessageConfirmationView.as_view(), name="message_confirmation"),
]
