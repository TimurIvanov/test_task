from uuid import uuid4

from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED

from .models import CustomUser, Message, MessageToken
from .serializer import MessageSerializer, MessageConfirmationSerializer
from .service.producer import prod


class MessageView(GenericAPIView):
    serializer_class = MessageSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        if serializer.is_valid():
            user = CustomUser.objects.get(id=data.get('user_id'))
            message = Message.objects.create(text=data.get('text'), user=user)
            message.save()
            token = MessageToken.objects.create(token=uuid4(), message=message)
            token.save()
            prod(message, token.token)
            return Response({'status': 'сообщение отправлено'}, status=HTTP_200_OK)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class MessageConfirmationView(GenericAPIView):
    serializer_class = MessageConfirmationSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        if serializer.is_valid():
            auth_token = request.META.get('HTTP_AUTHORIZATION')
            message_id = data.get('message_id')
            success = data.get('success')
            message = Message.objects.filter(id=message_id).first()
            token = MessageToken.objects.filter(message_id=message_id).first()
            if auth_token != token.token:
                return Response({'status': 'unauthorized'}, status=HTTP_401_UNAUTHORIZED)
            if success == 'True':
                message.status = 'blocked'
                message.save()
                return Response({'status': 'blocked'}, status=HTTP_200_OK)
            elif success == 'False':
                message.status = 'correct'
                message.save()
                return Response({'status': 'correct'}, status=HTTP_200_OK)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
