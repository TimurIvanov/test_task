from django.contrib import admin
from .models import CustomUser, Message, MessageToken
# Register your models here.

admin.site.register(CustomUser)
admin.site.register(Message)
admin.site.register(MessageToken)
