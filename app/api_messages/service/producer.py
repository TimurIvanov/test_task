from django.http import HttpResponse

from kafka import KafkaProducer
import pickle


def prod(msg, msg_token):
    producer = KafkaProducer(bootstrap_servers='127.0.0.1:9092')
    data = {
        'msg_token': msg_token,
        'msg_id': msg.id,
        'msg': msg.text
    }
    serialized_data = pickle.dumps(data, pickle.HIGHEST_PROTOCOL)
    producer.send('msg_topic', serialized_data)
    print('message was sent')
    return HttpResponse(200)
