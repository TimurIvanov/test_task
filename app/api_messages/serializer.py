from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Message, CustomUser


class MessageSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.id')

    def validate_user_id(self, user_id):
        user, created = CustomUser.objects.get_or_create(id=user_id)
        if created:
            user.name = "User"
            user.save()

        return user_id

    class Meta:
        model = Message
        fields = ['text', 'user_id']


class MessageConfirmationSerializer(serializers.Serializer):
    message_id = serializers.CharField(source='message.id')
    success = serializers.CharField(max_length=10)

    def validate_message_id(self, message_id):
        message = Message.objects.filter(id=message_id).first()
        if message:
            return message_id
        else:
            raise ValidationError('Сообщения с таким id не существует')