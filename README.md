# test_task



## Getting started

- Склонируйте репозиторий (git clone https://gitlab.com/TimurIvanov/test_task.git)
- Создайте виртуальное окружение (python -m venv .venv)
- Установите зависимости (pip install -r requiremrnts.txt)
- Установите переменные окружения DB_NAME, DB_USER, DB_PASS, DB_HOST, DB_PORT
- Прогоните миграции (python manage.py makemigrations & python manage.py migrate)
- Запустите локально кафку
- Запустите app (python manage.py runserver)
- Запустите consumer (python consumer.py)



Docker-compose сделать не получилось, вроде все запускалось, но consumer не принимал сообщения. Локально все работает.
